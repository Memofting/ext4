from src.file_system.FileSystem import FileSystem
import sys
import click
from click import Abort, ClickException, BadParameter, MissingParameter
from itertools import islice
import threading

PATH = "PATH"
FILESYSTEM = "FILESYSTEM"


@click.group(invoke_without_command=True)
@click.argument("filename")
def cli(filename):
    """
    CLI for ext4 decoding tools.
    """
    with open(filename, "br") as fp:
        fs = FileSystem(fp)
        while True:
            args = click.prompt(fs.get_hello_message(), prompt_suffix="$ ").split()

            # чтобы после вывода --help у нас не падала программа
            t = threading.Thread(target=invoke_command,
                                 args=(args, fs))
            t.start()
            t.join()


def invoke_command(args, fs):

    try:
        name = args[0]
        command = _map.get(name)
        if command == None:
            raise BadParameter("Unknown command: %s" % name)

        params = args[1:]

        ctx = command.make_context(name, params)
        ctx.obj = {}
        ctx.obj[FILESYSTEM] = fs
        with ctx:
            command.invoke(ctx)

    except ClickException as ex:
        ex.show()
    except Abort:
        pass


@cli.command("ls", short_help="list subfolders and files")
@click.option("-m", "--max", "max_", type=int, default=90, show_default=True,
              help="It describes amount of MAX entities in the out")
@click.argument("source", default=".")
@click.pass_context
def ls(ctx, source, max_):
    _fs = ctx.obj[FILESYSTEM]
    click.echo("\n".join(islice(_fs.ls(), max_)))


@cli.command("help", short_help="list available commands")
def help_():
    """
    ls - list subdirectories and files
    cd - change directory
    help - print this message
    """
    pass

@cli.command("cd", short_help="change directory")
@click.argument("source", default=".")
@click.pass_context
def cd(ctx, source):
    _fs = ctx.obj[FILESYSTEM]
    _fs.cd(source)


_map = {
    "ls": ls,
    "cd": cd,
    "help": help_
}

if __name__ == "__main__":
    args = sys.argv[1:]
    while True:
        try:
            ctx = cli.make_context("cli", args)
            with ctx:
                cli.invoke(ctx)

        except Abort:
            print()
            sys.exit(0)
        except MissingParameter:
            args = ["--help"]
