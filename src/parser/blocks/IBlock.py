class IBlock:
    _format = ""
    _size = 0
    _names = []

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            if not k.startswith("_"):
                setattr(self, k, v)

    def __str__(self):
        res = list(map(tuple, self.__dict__.items()))
        sorted(res)
        return "\n".join(map("{0[0]:<30}: {0[1]}".format, res))

    def __repr__(self):
        return str(self)