import struct
import bisect
import time

from src.parser import ParserFunctions
from src.parser.blocks.DirectoryEntry.DirectoryEntry import DirectoryEntry
from src.parser.blocks.DirectoryEntry.DirectoryEntry2 import DirectoryEntry2
from src.parser.blocks.ExtentTree.Extent import Extent
from src.parser.blocks.ExtentTree.ExtentTreeIndex import ExtentTreeIndex
from src.parser.blocks.ExtentTree.ExtentTreeHeader import ExtentTreeHeader
from src.parser.blocks.HashTree.DxRoot import DxRoot
from src.parser.blocks.IBlock import IBlock


class Inode(IBlock):
    S_EXTENT_TREE_INDEX_DESCRIPTOR = "Extent tree index"
    S_EXTENT_TREE_HEADER_DESCRIPTOR = "Extent tree header"
    S_CHECKSUM_DESCRIPTOR = "Checksum"
    S_EXTENT_DESCRIPTOR = "Extent"
    PHYSICAL_BLOCK_SIZE = 512

    _format = "<2H5L2H3L60s4L8H7L"
    _size = 160
    _names = [
        "i_mode",
        "i_uid",
        "i_size_lo",
        "i_atime",
        "i_ctime",
        "i_mtime",
        "i_dtime",
        "i_gid",
        "i_links_count",
        "i_blocks_lo",
        "i_flags",
        "l_i_version",
        "i_block",
        "i_generation",
        "i_file_acl_lo",
        "i_size_high",
        "i_obso_faddr",
        "l_i_blocks_high",
        "l_i_file_acl_high",
        "l_i_uid_high",
        "l_i_gid_high",
        "l_i_checksum_lo",
        "l_i_reserved",
        "i_extra_isize",
        "i_checksum_hi",
        "i_ctime_extra",
        "i_mtime_extra",
        "i_atime_extra",
        "i_crtime",
        "i_crtime_extra",
        "i_version_hi",
        "i_projid",
    ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.extra_inf = None

        # это символьная ссылка
        # if (self.i_mode & 0xA000 and not self.i_mode & 0x8000):
        #     self.i_block = self.i_block.decode()

        # включен режим inline и первые 60 байтов
        # данных файла хранятся в этом поле

        # if self.i_flags & 0x10000000:

    def set_extra_information(self, extra_information):
        self.extra_inf = extra_information

    def get_file_extents(self, fp, sb):
        _extents = self._gather_extents(fp, sb)

        return sorted(_extents[Inode.S_EXTENT_DESCRIPTOR], key=lambda x: x.ee_block)

    def get_actual_disk_space(self, fp, sb):
        a_extents = self.get_file_extents(fp, sb)
        return sum(map(lambda x: len(x), a_extents)) * sb.s_block_size

    def get_dir_entries(self, fp, sb):
        """
        Возращает итератор по содержумому каталогов. Для каталогов, которые
        поддерживают hashtree выводит содержимое в линейном порядке

        :param fp: открытый файловый объект
        :param sb: ссылка на SUperBlock
        :return: итератор по DirEntry каталогов
        """
        if self.i_flags & 0x1000:
            return self._dir_hash_tree(fp, sb)
        else:
            return self._dir_linear(fp, sb)

    def _dir_linear(self, fp, sb):
        a_extents = self.get_file_extents(fp, sb)

        # нашли нужные нам extent и начинаем
        # декодировать содержимое блоков данных

        return self._parse_directory_entries(fp, sb, a_extents)

    def _parse_directory_entries(self, fp, sb, extents):
        # extents - должны быть отсортированы по порядку описания объекта данных

        for extent in extents:
            # _extent = _extents[0]
            _block_number = (extent.ee_start_hi << 32) + extent.ee_start_lo
            fp.seek(_block_number * sb.s_block_size)

            blocks = fp.read(sb.s_block_size * extent.ee_len)

            _directory_class = DirectoryEntry

            # directory entry record the file type
            if sb.s_feature_incompat & 0x2:
                _directory_class = DirectoryEntry2

            gen = ParserFunctions.get_n_len_byte_string(blocks)
            next(gen)
            while True:
                res = _directory_class.parse(gen)
                if not res:
                    break
                yield res


    def _parse_root_hash_tree(self, fp, sb, extent):
        _block_number = (extent.ee_start_hi << 32) + extent.ee_start_lo
        fp.seek(_block_number * sb.s_block_size)

        blocks = fp.read(sb.s_block_size * extent.ee_len)
        gen = ParserFunctions.get_n_len_byte_string(blocks)
        next(gen)

        return DxRoot.parse(gen)

    def _gather_extents(self, fp, sb):
        extents = self.get_extents()
        # спроси про формат этого поля в подкаталогах


        if len(extents.values()) < 3:
            raise AttributeError("Inode не поддерживает directory entries")


        _tree_depth = extents[Inode.S_EXTENT_TREE_HEADER_DESCRIPTOR].eh_depth
        if _tree_depth == 1:
            # если у нас в описание подпапки были только index-ы, то мы,
            # пройдемпо тем  данным, что они хранят, и достанем оттуда
            # нужные нам extents и добавим в объект

            extents[Inode.S_EXTENT_DESCRIPTOR] = []
            for index in extents[Inode.S_EXTENT_TREE_INDEX_DESCRIPTOR]:
                _index_block_number = (index.ei_leaf_hi << 32) + index.ei_leaf_lo
                fp.seek(_index_block_number * sb.s_block_size)
                _block = fp.read(sb.s_block_size)

                _extents = self._get_extents(_block)
                extents[Inode.S_EXTENT_DESCRIPTOR].extend(_extents[Inode.S_EXTENT_DESCRIPTOR])
        elif _tree_depth > 1:
            # случай, который не используется на практике
            message = ("Алгоритм не поддерживает работу с глубиной дерева, "
                       "отвечающего фрагментации блока данных, "
                       "соответствующего inode: '{}', больше чем 1, а здесь "
                       "она: '{}'", self.number, _tree_depth)
            raise NotImplementedError(message)

        return extents

    def _gather_extents_hash(self, fp, sb):
        _extents = self.get_extents()
        extents = self._gather_extents(fp, sb)

        # так как здесь всего одна Extents
        return extents[Inode.S_EXTENT_DESCRIPTOR][0]

    def get_extents(self):
        # сейчас реализация extent-ов учитывает только случаи, когда map на
        # файл содержится в первых четырех inode-ах в i_block

        # флаг, который включает Extent Tree (проверка на корректность)
        if not self.i_flags & 0x80000:
            raise AttributeError("Inode не поддерживает extent tree.")
        return Inode._get_extents(self.i_block)

    @staticmethod
    def _get_extents(block):
        gen = ParserFunctions.get_n_len_byte_string(block)
        next(gen)
        header_data = gen.send(ExtentTreeHeader.get_size())

        _eh = ExtentTreeHeader.parse(header_data)
        _descriptor, _elements = "", []

        if _eh.eh_depth > 0:
            _element_class = ExtentTreeIndex
            _descriptor = Inode.S_EXTENT_TREE_INDEX_DESCRIPTOR
        else:
            _element_class = Extent
            _descriptor = Inode.S_EXTENT_DESCRIPTOR

        for i in range(_eh.eh_entries):
            next(gen)
            element_data = gen.send(_element_class.get_size())
            _elements.append(_element_class.parse(element_data))

        next(gen)
        _check_sum = struct.unpack("<L", gen.send(4))[0]

        return {
            Inode.S_EXTENT_TREE_HEADER_DESCRIPTOR: _eh,
            _descriptor: _elements,
            Inode.S_CHECKSUM_DESCRIPTOR: _check_sum
        }


    def _dir_hash_tree(self, fp, sb):
        extent = self._gather_extents_hash(fp, sb)

        root = self._parse_root_hash_tree(fp, sb, extent)
        hash_tree_entries = root.entries

        yield root.dot
        yield root.dotdot


        _block_number = (extent.ee_start_hi << 32) + extent.ee_start_lo
        _start = _block_number * sb.s_block_size


        for entry in hash_tree_entries:
            fp.seek(_start + (entry.block) * sb.s_block_size)
            blocks = fp.read(sb.s_block_size * extent.ee_len)

            _directory_class = DirectoryEntry

            # directory entry record the file type
            if sb.s_feature_incompat & 0x2:
                _directory_class = DirectoryEntry2

            gen = ParserFunctions.get_n_len_byte_string(blocks)
            next(gen)
            while True:
                res = _directory_class.parse(gen)
                if not res:
                    break
                yield res

    def get_filemode(self):
        masks = (0x100, 0x80, 0x40, 0x20, 0x10, 0x8, 0x4, 0x2, 0x1)
        on_match = ('r', 'w', 'x', 'r', 'w', 'x', 'r', 'w', 'x')

        return Inode.get_permissions_str(self.i_mode, masks, on_match)

    def get_time(self):
        # вычисляет последнее время доступа к inode
        # поддерживает корректную работу только диапазона
        # 1970-01-01 to 2038-01-19
        return time.strftime("%b %d %H:%M", time.gmtime(self.i_atime))

    def get_user_uid(self):
        return self.i_uid


    @staticmethod
    def get_permissions_str(mode, masks, on_match):
        res = ""
        for mask, symbol in zip(masks, on_match):
            if mode & mask:
                res += symbol
            else:
                res += "-"
        return res


    def ls_print(self):
        return "{} ".format(self.get_filemode())

    @staticmethod
    def find_inode(fp, n, sb, inode_tables):
        shift = (n - 1) % sb.s_inodes_per_group
        group = (n - 1) // sb.s_inodes_per_group

        inode_table_index = max(bisect.bisect(inode_tables, (group, 0)) - 1, 0)
        inode_table_shift = inode_tables[inode_table_index]

        fp.seek(inode_table_shift[1] * sb.s_block_size + shift * sb.s_inode_size)
        return Inode.parse(fp, n)

    @staticmethod
    def parse(fp, number):
        res = struct.unpack(Inode._format, fp.read(Inode._size))
        inode = Inode(**dict(zip(Inode._names, res)), number=number)
        inode.set_extra_information(fp.read(inode.i_extra_isize))
        return inode
