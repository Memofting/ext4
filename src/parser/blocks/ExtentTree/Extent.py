import struct

from src.parser.blocks.IBlock import IBlock


class Extent(IBlock):
    _format = "<L2HL"
    _size = 12
    _names = [
        "ee_block",
        "ee_len",
        "ee_start_hi",
        "ee_start_lo",
    ]

    def __len__(self):
        return self.ee_len & (~0x8000)

    def is_initialized(self):
        return self.ee_len  & 32768

    @staticmethod
    def get_size():
        return Extent._size

    @staticmethod
    def parse(buf):
        res = struct.unpack(Extent._format, buf)
        _ei = Extent(**dict(zip(Extent._names, res)))
        return _ei
