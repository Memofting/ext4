import struct

from src.parser.blocks.IBlock import IBlock


class ExtentTreeIndex(IBlock):
    _format = "<2L2H"
    _size = 12
    _names = [
        "ei_block",
        "ei_leaf_lo",
        "ei_leaf_hi",
        "ei_unused",
    ]

    @staticmethod
    def get_size():
        return ExtentTreeIndex._size

    @staticmethod
    def parse(buf):
        res = struct.unpack(ExtentTreeIndex._format, buf)
        _ei = ExtentTreeIndex(**dict(zip(ExtentTreeIndex._names, res)))
        return _ei
