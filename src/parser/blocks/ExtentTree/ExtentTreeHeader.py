import struct

from src.parser.blocks.IBlock import IBlock


class ExtentTreeHeader(IBlock):
    _format = "<4HL"
    _size = 12
    _names = [
        "eh_magic",
        "eh_entries",
        "eh_max",
        "eh_depth",
        "eh_generation",
    ]

    @staticmethod
    def get_size():
        return ExtentTreeHeader._size

    @staticmethod
    def parse(buf):
        res = struct.unpack(ExtentTreeHeader._format, buf)
        _eh = ExtentTreeHeader(**dict(zip(ExtentTreeHeader._names, res)))
        return _eh
