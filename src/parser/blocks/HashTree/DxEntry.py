import struct
from src.parser.blocks.IBlock import IBlock


class DxEntry(IBlock):
    _format = "<4sL"
    _size = 8
    _names = [
        "hash",
        "block"
    ]

    @staticmethod
    def parse(gen):
        buf = gen.send(DxEntry._size)
        # if not _entries:
        #     return None
        next(gen)

        return DxEntry(**dict(zip(DxEntry._names, struct.unpack(DxEntry._format, buf))))