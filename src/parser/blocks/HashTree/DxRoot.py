import struct

from src.parser.blocks.HashTree.Dot import Dot
from src.parser.blocks.HashTree.DxEntry import DxEntry
from src.parser.blocks.IBlock import IBlock


class DxRoot(IBlock):



    _format = "<L4B2HL"
    _names = [
        "_",
        "hash_version",
        "info_length",
        "indirect_levels",
        "_",
        "limit",
        "count",
        "block"
    ]
    _size = 16

    @staticmethod
    def parse(gen):
        _dot = Dot.parse(gen)
        _dotdot = Dot.parse(gen)

        _buf = gen.send(DxRoot._size)
        next(gen)

        # depth_of_tree - если это поле == 0х00, оно определяет плоское дерево
        # на практике деревья практически всегда плоские

        _fields = struct.unpack(DxRoot._format, _buf)
        root = DxRoot(**dict(zip(DxRoot._names, _fields)),
                      dot=_dot, dotdot=_dotdot)
        entries = [DxEntry(**dict(zip(DxEntry._names, (b'\x00\x00\x00\x00', root.block))))]

        for _ in range(root.count):
            entries.append(
                DxEntry.parse(gen))

        # считаем
        # assert len(entries) == root.count + 1

        root.entries = entries
        return root