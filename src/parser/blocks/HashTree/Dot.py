import struct
from src.parser.blocks.IBlock import IBlock


class Dot(IBlock):
    _format = "<LH2B4s"
    _names = [
        "inode",
        "rec_len",
        "name_len",
        "file_type",
        "name"
    ]
    _size = 12

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = self.name[:self.name_len].decode()

    def get_type_descriptor(self):
        return "d"

    @staticmethod
    def parse(gen):
        buf = struct.unpack(Dot._format, gen.send(Dot._size))
        next(gen)

        return Dot(**dict(zip(Dot._names, buf)))