import struct
from math import ceil

from src.parser.blocks.BlockGroupDescriptor import BlockGroupDescriptor
from src.parser.blocks.IBlock import IBlock


class SuperBlock(IBlock):
    _format = "<13L6H4L2HL2H3L16s16s64sL2BH16s3L16s2BH3L68s3L2HL2HQL2BHQ2LQ4LQ32s4LQ32s64s3L8s4s16s3L392sL"
    _size = 1024
    _names = [
        "s_inodes_count",
        "s_blocks_count_lo",
        "s_r_blocks_count_lo",
        "s_free_blocks_count_lo",
        "s_free_inodes_count",
        "s_first_data_block",
        "s_log_block_size",
        "s_log_cluster_size",
        "s_blocks_per_group",
        "s_clusters_per_group",
        "s_inodes_per_group",
        "s_mtime",
        "s_wtime",
        "s_mnt_count",
        "s_max_mnt_count",
        "s_magic",
        "s_state",
        "s_errors",
        "s_minor_rev_level",
        "s_lastcheck",
        "s_checkinterval",
        "s_creator_os",
        "s_rev_level",
        "s_def_resuid",
        "s_def_resgid",
        "s_first_ino",
        "s_inode_size",
        "s_block_group_nr",
        "s_feature_compat",
        "s_feature_incompat",
        "s_feature_ro_compat",
        "s_uuid",
        "s_volume_name",
        "s_last_mounted",
        "s_algorithm_usage_bitmap",
        "s_prealloc_blocks",
        "s_prealloc_dir_blocks",
        "s_reserved_gdt_blocks",
        "s_journal_uuid",
        "s_journal_inum",
        "s_journal_dev",
        "s_last_orphan",
        "s_hash_seed",
        "s_def_hash_version",
        "s_jnl_backup_type",
        "s_desc_size",
        "s_default_mount_opts",
        "s_first_meta_bg",
        "s_mkfs_time",
        "s_jnl_blocks",
        "s_blocks_count_hi",
        "s_r_blocks_count_hi",
        "s_free_blocks_count_hi",
        "s_min_extra_isize",
        "s_want_extra_isize",
        "s_flags",
        "s_raid_stride",
        "s_mmp_interval",
        "s_mmp_block",
        "s_raid_stripe_width",
        "s_log_groups_per_flex",
        "s_checksum_type",
        "s_reserved_pad",
        "s_kbytes_written",
        "s_snapshot_inum",
        "s_snapshot_id",
        "s_snapshot_r_blocks_count",
        "s_snapshot_list",
        "s_error_count",
        "s_first_error_time",
        "s_first_error_ino",
        "s_first_error_block",
        "s_first_error_func",
        "s_first_error_line",
        "s_last_error_time",
        "s_last_error_ino",
        "s_last_error_line",
        "s_last_error_block",
        "s_last_error_func",
        "s_mount_opts",
        "s_usr_quota_inum",
        "s_grp_quota_inum",
        "s_overhead_blocks",
        "s_backup_bgs",
        "s_encrypt_algos",
        "s_encrypt_pw_salt",
        "s_lpf_ino",
        "s_prj_quota_inum",
        "s_checksum_seed",
        "s_reserved",
        "s_checksum",
    ]

    def __init__(self, **kwargs):
        # print(kwargs)
        super().__init__(**kwargs)

        self.s_block_size = 2 ** (10 + self.s_log_block_size)

        # описывает количество блоков, которые будут заняти дескрипторами групп
        # self.s_group_descriptors = ceil(self.s_blocks_count_lo / self.s_blocks_per_group * self.s_desc_size / self.s_block_size)

        self.s_amount_group_descriptors = ceil(((self.s_blocks_count_hi << 32) + self.s_blocks_count_lo) / self.s_blocks_per_group)
        self.s_group_descriptors = ceil(self.s_amount_group_descriptors * BlockGroupDescriptor._size / self.s_block_size)

        self.s_uuid = struct.unpack("<16B", self.s_uuid)
        self.s_volume_name = self.s_volume_name.decode()
        self.s_last_mounted = self.s_last_mounted.decode()
        self.s_journal_uuid = struct.unpack("<16B", self.s_journal_uuid)
        self.s_hash_seed = struct.unpack("<4L", self.s_hash_seed)
        self.s_jnl_blocks = struct.unpack("<17L", self.s_jnl_blocks)
        self.s_first_error_func = struct.unpack("<32B", self.s_first_error_func)
        self.s_last_error_func = struct.unpack("<32B", self.s_last_error_func)
        self.s_mount_opts = struct.unpack("<64B", self.s_mount_opts)
        self.s_backup_bgs = struct.unpack("<2L", self.s_backup_bgs)
        self.s_encrypt_algos = struct.unpack("<4B", self.s_encrypt_algos)
        self.s_encrypt_pw_salt = struct.unpack("<16B", self.s_encrypt_pw_salt)
        self.s_reserved = struct.unpack("<98L", self.s_reserved)

    @staticmethod
    def parse(fp):

        res = struct.unpack(SuperBlock._format, fp.read(SuperBlock._size))
        return SuperBlock(**dict(zip(SuperBlock._names, res)))
