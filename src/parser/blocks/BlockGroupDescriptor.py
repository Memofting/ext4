import struct

from src.parser.blocks.IBlock import IBlock


class BlockGroupDescriptor(IBlock):
    _format = "<3L4HL4H3L4HL2HL"
    _size = 64
    _names = [
        "bg_block_bitmap_lo",
        "bg_inode_bitmap_lo",
        "bg_inode_table_lo",
        "bg_free_blocks_count_lo",
        "bg_free_inodes_count_lo",
        "bg_used_dirs_count_lo",
        "bg_flags",
        "bg_exclude_bitmap_lo",
        "bg_block_bitmap_csum_lo",
        "bg_inode_bitmap_csum_lo",
        "bg_itable_unused_lo",
        "bg_checksum",
        "bg_block_bitmap_hi",
        "bg_inode_bitmap_hi",
        "bg_inode_table_hi",
        "bg_free_blocks_count_hi",
        "bg_free_inodes_count_hi",
        "bg_used_dirs_count_hi",
        "bg_itable_unused_hi",
        "bg_exclude_bitmap_hi",
        "bg_block_bitmap_csum_hi",
        "bg_inode_bitmap_csum_hi",
        "bg_reserved",
    ]

    @staticmethod
    def parse(data):
        res = struct.unpack(BlockGroupDescriptor._format, data)
        return BlockGroupDescriptor(
            **dict(zip(BlockGroupDescriptor._names, res)))
