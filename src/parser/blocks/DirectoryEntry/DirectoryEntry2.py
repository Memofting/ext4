import struct
from src.parser.blocks.IBlock import IBlock


class DirectoryEntry2(IBlock):
    _format = "<LH2B"
    _size = 8
    _n_format = "<{}s"
    _names = [
        "inode",
        "rec_len",
        "name_len",
        "file_type",
        "name",
        "hidden_data"
    ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = self.name.decode("utf-8")

    def get_type_descriptor(self):
        if self.file_type == 0x7:
            return "l"
        elif self.file_type == 0x2:
            return "d"
        else:
            return "-"

    @staticmethod
    def parse(b_generator):
        _h_buf = b_generator.send(DirectoryEntry2._size)
        if not _h_buf:
            return None
        next(b_generator)

        _header = struct.unpack(DirectoryEntry2._format, _h_buf)
        _id, _rl, _nl, _ft = _header
        if not _id:
            return None
        residual_length = _rl - DirectoryEntry2._size

        _n_buf = b_generator.send(residual_length)
        next(b_generator)

        _b_name = struct.unpack(DirectoryEntry2._n_format.format(residual_length), _n_buf)[0]
        _name, _hidden_data = _b_name[:_nl], _b_name[_nl:]
        return DirectoryEntry2(**dict(zip(DirectoryEntry2._names, (_id, _rl, _nl, _ft, _name, _hidden_data))))