import struct
from src.parser.blocks.IBlock import IBlock


class DirectoryEntry(IBlock):
    _format = "<L2H"
    _size = 8
    _n_format = "<{}s"
    _names = [
        "inode",
        "rec_len",
        "name_len",
        "name"
    ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = self.name.decode("utf-8")

    @staticmethod
    def parse(b_generator):
        _h_buf = b_generator.send(DirectoryEntry._size)
        if not _h_buf:
            return None
        next(b_generator)

        _header = struct.unpack(DirectoryEntry._format, _h_buf)
        _id, _rl, _nl, = _header
        if not _id:
            return None
        residual_length = _rl - DirectoryEntry._size

        _n_buf = b_generator.send(residual_length)
        next(b_generator)

        _b_name = struct.unpack(DirectoryEntry._n_format.format(residual_length), _n_buf)[0]
        _name = _b_name[:_nl]
        return DirectoryEntry(**dict(zip(DirectoryEntry._names, (_id, _rl, _nl, _name))))
