from itertools import islice


def get_n_len_byte_string(buf):
    # buf - переменная, которая содержит прочтенную в память строку байтов

    # мы делаем так, чтобы гарантировать, что при последующих выховах мы
    # будем проходить по буферу по порядку байтов
    i_buf = iter(buf)

    while True:
        n = yield
        # print(n)
        res = bytes(islice(i_buf, n))
        yield res
