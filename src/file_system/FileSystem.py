from itertools import islice
from src.parser.blocks.Inode import Inode
from src.parser.blocks.SuperBlock import SuperBlock
from src.parser.blocks.BlockGroupDescriptor import BlockGroupDescriptor


class FileSystem:
    HELLO_FORMAT = "{}"
    LS_FORMAT = "{inode:>6}  {perm}  {uid:<7}  {size:>10}  {time:<14}  {name}"
    MAX_OUT = 100

    def __init__(self, fp):
        self._fp = fp
        self.sb = None
        self._parse()
        self.l_hello_messages = ["/"]

        # начинаем с корня
        self.cur_dir_inode = Inode.find_inode(self._fp, 2, self.sb, self.inode_table_addresses)


    def _parse(self):
        self.read_first_group()

    def read_first_group(self):
        # читаем первую группу
        # хотим прочитать inode root-а

        self._fp.read(1024)
        self.sb = SuperBlock.parse(self._fp)

        # переместилсь на второй блок
        self._fp.seek((self.sb.s_first_data_block + 1) * self.sb.s_block_size)

        # пропустить все дескрипторы групп

        _start = self._fp.tell()

        bgt = {}
        for i in range(self.sb.s_amount_group_descriptors):
            bg_buf = self._fp.read(BlockGroupDescriptor._size)
            bgt[i] = BlockGroupDescriptor.parse(bg_buf)

        self._fp.seek(_start + self.sb.s_group_descriptors * self.sb.s_block_size)


        # сохраним адреса таблиц с inode-ами

        self.inode_table_addresses = FileSystem.get_inode_table_addresses(bgt, self.sb)

        # пропустить все reserved gdt blocks

        # self._fp.seek(self.sb.s_reserved_gdt_blocks * self.sb.s_block_size, 1)
        #
        # groups_per_flex = 2 ** self.sb.s_log_groups_per_flex
        #
        # # чтение data block bitmap (с поддержкой flexible mode)
        # self._fp.seek(self.sb.s_block_size * groups_per_flex, 1)
        #
        # # чтение inode bitmap (с поддержкой flexible mode)
        # self._fp.seek(self.sb.s_block_size * groups_per_flex, 1)

    def ls(self):
        # научились находить inode table по номеру inode

        for sub_inode, dir_entry in self._iter_dir_entries(self.cur_dir_inode):

            yield (FileSystem.LS_FORMAT.format(
                perm=dir_entry.get_type_descriptor() + sub_inode.get_filemode(),
                time=sub_inode.get_time(),
                name=dir_entry.name,
                uid=sub_inode.get_user_uid(),
                size=(sub_inode.i_size_high << 32) + sub_inode.i_size_lo,
                inode=dir_entry.inode
            ))


    def cd(self, folder):
        """
        Модифицирует поля:
            self.cur_dir_inode
            self.l_hello_message
        """
        for inode, entry in self._iter_dir_entries(self.cur_dir_inode):
            if entry.name == folder and entry.get_type_descriptor() == "d":
                self.cur_dir_inode = inode

                path = folder.split("/")

                # удаляем пустую строку в конце
                if not path[-1]:
                    path = path[:-1]

                # переходим внутри каталога
                # поставить обработку . и ..
                if not path[0]:
                    path = path[1:]

                self.l_hello_messages.extend(path)

                break
        else:
            return "Не существует папки: {}".format(folder)

    def get_hello_message(self):
        return FileSystem.HELLO_FORMAT.format("/".join(self.l_hello_messages))

    def _iter_dir_entries(self, inode, is_full=False):
        entries = sorted(inode.get_dir_entries(self._fp, self.sb), key=lambda e: e.name)

        if not is_full:
            entries = islice(entries, FileSystem.MAX_OUT)

        for dir_entry in entries:
            sub_inode = Inode.find_inode(self._fp, dir_entry.inode, self.sb, self.inode_table_addresses)

            yield sub_inode, dir_entry

    @staticmethod
    def extract_inode_address(bg):
        return (bg.bg_inode_table_hi << 32) + bg.bg_inode_table_lo

    @staticmethod
    def get_inode_table_addresses(bgt, sb):
        res = []
        for num, bg in bgt.items():
            res.append((num, FileSystem.extract_inode_address(bg)))
        return sorted(res)